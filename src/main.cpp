
#include "toy_cipher.hpp"

#include <iostream>

using namespace toy_cipher;

int main() {

  Toy toy(to<data>("I'm an initialization vector"), 0b10101010);

  std::string input_text = "Encryption is basically the method of disguising plain "
                           "or clear text in such a way as to hide its contents from "
                           "anyone for whom it is not intended.";

  auto encripted = toy.encrypt(to<data>(input_text));
  auto decripted = toy.decrypt(encripted);
  auto stringified = to<std::string>(decripted);

  std::cout << input_text << "\n"
            << stringified << "\n";

}