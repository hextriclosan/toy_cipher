


set(SRC
  toy_test.cpp
)

set (TEST_NAME ${PROJECT_NAME}_test)
add_executable(${TEST_NAME} ${SRC})
target_link_libraries(${TEST_NAME} ${LIBRARY_NAME} gtest gtest_main)
set_target_properties (${TEST_NAME} PROPERTIES FOLDER tests)

if (CODE_COVERAGE)

setup_target_for_coverage_lcov(
  NAME testrunner_coverage_html  # New target name
  EXECUTABLE ${TEST_NAME}   # Executable in PROJECT_BINARY_DIR
  DEPENDENCIES ${TEST_NAME} # Dependencies to build first
)

setup_target_for_coverage_gcovr_xml(
  NAME testrunner_coverage_xml    # New target name
  EXECUTABLE ${TEST_NAME}         # Executable in PROJECT_BINARY_DIR
  DEPENDENCIES ${TEST_NAME}  # Dependencies to build first
)

endif()
