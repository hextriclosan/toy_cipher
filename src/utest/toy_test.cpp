#include "gtest/gtest.h"

#include "toy_cipher.hpp"

namespace toy_cipher {

namespace {
data text = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '1', '2', '3', '4', '5', '6', '7', '8' };
Toy::data_block block = { {'a', 'b', 'c', 'd'}, { 'e', 'f', 'g', 'h' }, { '1', '2', '3', '4' }, { '5', '6', '7', '8' } };
Toy::data_block flipped_block = { { '1', '2', '3', '4' }, { '5', '6', '7', '8' }, {'a', 'b', 'c', 'd'}, { 'e', 'f', 'g', 'h' } };
}

struct ToyTest : public Toy, public ::testing::Test {
  ToyTest() : Toy({}, {})
  {}
};

TEST_F(ToyTest, ConstructBlock) {

  EXPECT_EQ(block, construct_block(std::begin(text), std::end(text)));
}

TEST_F(ToyTest, DeconstructBlock) {

  EXPECT_EQ(text, deconstruct_block(block));
}

TEST_F(ToyTest, XorBlocks) {
  Toy::data_block result = { { 0x50, 0x50, 0x50, 0x50 }, { 0x50, 0x50, 0x50, 0x50 }, { 0x50, 0x50, 0x50, 0x50 }, { 0x50, 0x50, 0x50, 0x50 } };
  EXPECT_EQ(result, xor_blocks(block, flipped_block));
}

TEST_F(ToyTest, Permutation0) {
  {
    key_ = ~0;
    auto block0(block);
    Toy::data_block result_true = { {'d', 'c', 'b', 'a'}, { 'h', 'g', 'f', 'e' }, { '4', '3', '2', '1' }, { '8', '7', '6', '5' } };
    permutation_0(block0);
    EXPECT_EQ(result_true, block0);

    permutation_0(block0);
    EXPECT_EQ(block, block0);
  }

  {
    key_ = 0;
    auto block0(block);
    Toy::data_block result_false = { {'c', 'd', 'a', 'b'}, { 'g', 'h', 'e', 'f' }, { '3', '4', '1', '2' }, { '7', '8', '5', '6' } };
    permutation_0(block0);
    EXPECT_EQ(result_false, block0);

    permutation_0(block0);
    EXPECT_EQ(block, block0);
  }
}

TEST_F(ToyTest, Permutation1) {
  {
    key_ = ~0;
    auto block0(block);
    Toy::data_block result_true = { { '5', '6', '7', '8' }, { '1', '2', '3', '4' }, { 'e', 'f', 'g', 'h' }, {'a', 'b', 'c', 'd'} };
    permutation_1(block0);
    EXPECT_EQ(result_true, block0);

    permutation_1(block0);
    EXPECT_EQ(block, block0);
  }

  {
    key_ = 0;
    auto block0(block);
    Toy::data_block result_false = { {'d', 'c', 'b', 'a'}, { 'h', 'g', 'f', 'e' }, { '4', '3', '2', '1' }, { '8', '7', '6', '5' } };
    permutation_1(block0);
    EXPECT_EQ(result_false, block0);

    permutation_1(block0);
    EXPECT_EQ(block, block0);
  }
}

TEST_F(ToyTest, Permutation2) {
  {
    key_ = ~0;
    auto block0(block);
    Toy::data_block result_true = { { 'e', 'f', 'g', 'h' }, {'a', 'b', 'c', 'd'}, { '5', '6', '7', '8' }, { '1', '2', '3', '4' } };
    permutation_2(block0);
    EXPECT_EQ(result_true, block0);

    permutation_2(block0);
    EXPECT_EQ(block, block0);
  }

  {
    key_ = 0;
    auto block0(block);
    Toy::data_block result_false = { { '1', '2', '3', '4' }, { '5', '6', '7', '8' }, {'a', 'b', 'c', 'd'}, { 'e', 'f', 'g', 'h' } };
    permutation_2(block0);
    EXPECT_EQ(result_false, block0);

    permutation_2(block0);
    EXPECT_EQ(block, block0);
  }
}

TEST_F(ToyTest, Permutation3) {
  {
    key_ = ~0;
    auto block0(block);
    Toy::data_block result_true = { { 'a', 0x3, 0x2, 0x5 }, { 'e', 0x3, 0x2, 0xd }, { '1', 0x3, 0x2, 0x5 }, { '5', 0x3, 0x2, 0xd } };
    permutation_3(block0);
    EXPECT_EQ(result_true, block0);

    permutation_3(block0);
    EXPECT_EQ(block, block0);
  }

  {
    key_ = 0;
    auto block0(block);
    Toy::data_block result_false = { { 0x3, 'b', 0x1, 0x6 }, { 0x3, 'f', 0x1, 0xe }, { 0x3, '2', 0x1, 0x6 }, { 0x3, '6', 0x1, 0xe } };
    permutation_3(block0);
    EXPECT_EQ(result_false, block0);

    permutation_3(block0);
    EXPECT_EQ(block, block0);
  }
}

TEST_F(ToyTest, Permutation4) {
  {
    key_ = ~0;
    auto block0(block);
    Toy::data_block result_true = { {'c', 'd', 'a', 'b'}, { 'g', 'h', 'e', 'f' }, { '3', '4', '1', '2' }, { '7', '8', '5', '6'} };
    permutation_4(block0);
    EXPECT_EQ(result_true, block0);

    permutation_4(block0);
    EXPECT_EQ(block, block0);
  }

  {
    key_ = 0;
    auto block0(block);
    Toy::data_block result_false = { { '1', '2', '3', '4' }, { '5', '6', '7', '8' }, {'a', 'b', 'c', 'd'}, { 'e', 'f', 'g', 'h' } };
    permutation_4(block0);
    EXPECT_EQ(result_false, block0);

    permutation_4(block0);
    EXPECT_EQ(block, block0);
  }
}

TEST_F(ToyTest, Permutation5) {
  {
    key_ = ~0;
    auto block0(block);
    Toy::data_block result_true = { { 'a', 0x3, 0x2, 0x5 }, { 'e', 0x3, 0x2, 0xd }, { '1', 0x3, 0x2, 0x5 }, { '5', 0x3, 0x2, 0xd } };
    permutation_5(block0);
    EXPECT_EQ(result_true, block0);

    permutation_5(block0);
    EXPECT_EQ(block, block0);
  }

  {
    key_ = 0;
    auto block0(block);
    Toy::data_block result_false = { {'a', 'b', 'c', 'd'}, { 0x4, 0x4, 0x4, 0xc }, { 0x50, 0x50, 0x50, 0x50 }, { 0x54, 0x54, 0x54, 0x5c } };
    permutation_5(block0);
    EXPECT_EQ(result_false, block0);

    permutation_5(block0);
    EXPECT_EQ(block, block0);
  }
}

TEST_F(ToyTest, Permutation6) {
  {
    key_ = ~0;
    auto block0(block);
    Toy::data_block result_true = { { 0x9e, 0x9d, 0x9c, 0x9b }, { 0x9a, 0x99, 0x98, 0x97 }, { 0xce, 0xcd, 0xcc, 0xcb }, { 0xca, 0xc9, 0xc8, 0xc7 } };
    permutation_6(block0);
    EXPECT_EQ(result_true, block0);

    permutation_6(block0);
    EXPECT_EQ(block, block0);
  }

  {
    key_ = 0;
    auto block0(block);
    Toy::data_block result_false = { { '1', '2', '3', '4' }, { '5', '6', '7', '8' }, {'a', 'b', 'c', 'd'}, { 'e', 'f', 'g', 'h' } };
    permutation_4(block0);
    EXPECT_EQ(result_false, block0);

    permutation_4(block0);
    EXPECT_EQ(block, block0);
  }
}

TEST_F(ToyTest, AddPadding) {
  const data empty_block({0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
  EXPECT_EQ(add_padding({}), empty_block);

  const data one_char({'X', 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
  EXPECT_EQ(add_padding({{'X'}}), one_char);

  const data two_blocks(32, 'Y');
  EXPECT_EQ(add_padding(data(32, 'Y')), two_blocks);
}

} // namespace toy_cipher
