#ifndef toy_cipher_h__
#define toy_cipher_h__

#include <bitset>
#include <vector>

namespace toy_cipher {


using byte = unsigned char;
using data = std::vector<byte>;

template<typename M, typename T>
M to(const T& t) {
  return M(std::begin(t), std::end(t));
}

class Toy {

public:
  using data_block = std::vector<data>;
  Toy(data iv, byte key) : key_(key) {
    iv.resize(BLOCK_SIZE);
    iv_ = construct_block(std::begin(iv), std::end(iv));
  }

  data encrypt(const data& text);
  data decrypt(const data& cipher);

protected:
  static data add_padding(const data& text);
  static data trim_padding(const data& text);
  static data_block construct_block(data::const_iterator b, data::const_iterator e);
  static data deconstruct_block(const data_block& block);
  static data_block xor_blocks(const data_block& first, const data_block& second);
  data_block encrypt_permutations(const data_block& block);
  data_block decrypt_permutations(const data_block& block);

  void permutation_0(data_block& block);
  void permutation_1(data_block& block);
  void permutation_2(data_block& block);
  void permutation_3(data_block& block);
  void permutation_4(data_block& block);
  void permutation_5(data_block& block);
  void permutation_6(data_block& block);
  void permutation_7(data_block& block);


  data_block iv_;
  std::bitset<8> key_;

  static const byte PADDING_SEPARATOR;
  static constexpr byte BLOCK_SIDE = 4;
  static constexpr byte BLOCK_SIZE = BLOCK_SIDE * BLOCK_SIDE;

};

}

#endif // toy_cipher_h__
