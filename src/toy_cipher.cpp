
#include "toy_cipher.hpp"

#include <algorithm>

namespace toy_cipher {

const byte Toy::PADDING_SEPARATOR = 0x80;

data Toy::encrypt(const data& text) {
  data result;
  data_block iv(iv_);
  auto input = add_padding(text);
  auto begin = std::begin(input);
  for (size_t i = 0; i < input.size() / BLOCK_SIZE; i++) {
    auto end = std::next(begin, BLOCK_SIZE);
    auto block = construct_block(begin, end);
    begin = end;

    auto xored_block = xor_blocks(block, iv);
    auto encrypted = encrypt_permutations(xored_block);
    iv = encrypted;
    auto deconstructed_block = deconstruct_block(encrypted);
    result.insert(std::end(result), std::begin(deconstructed_block), std::end(deconstructed_block));
  }

  return result;
}

data Toy::decrypt(const data& cipher) {
  data result;
  data_block iv(iv_);

  auto begin = std::begin(cipher);
  for (size_t i = 0; i < cipher.size() / BLOCK_SIZE; i++) {
    auto end = std::next(begin, BLOCK_SIZE);
    auto block = construct_block(begin, end);
    begin = end;

    auto decrypted = decrypt_permutations(block);
    auto xored_block = xor_blocks(decrypted, iv);
    iv = block;

    auto deconstructed_block = deconstruct_block(xored_block);
    result.insert(std::end(result), std::begin(deconstructed_block), std::end(deconstructed_block));
  }

  return trim_padding(result);
}

data Toy::add_padding(const data& text) {

  if (text.empty()) {
    data result(BLOCK_SIZE, 0);
    result.front() = PADDING_SEPARATOR;
    return result;
  }

  auto const lefts = text.size() % BLOCK_SIZE;
  if (lefts != 0) {
    auto t(text);
    t.push_back(PADDING_SEPARATOR);
    t.insert(std::end(t), BLOCK_SIZE - lefts - 1, 0);
    return t;
  }

  return text;
}

data Toy::trim_padding(const data& text) {

  auto rib = std::rbegin(text);
  auto rie = rib;
  std::advance(rie, BLOCK_SIZE - 1);
  size_t zeroes = 0;
  for (auto ri = rib; ri != rie; ++ri) {
    if (*ri == 0) {
      zeroes++;
    } else if(*ri == PADDING_SEPARATOR) {
      auto ib = std::begin(text);
      auto ei = ib;
      return data { ib, std::next(ei, text.size() - zeroes - 1) };
    } else {
      break;
    }
  }

  return text;
}

Toy::data_block Toy::construct_block(data::const_iterator b, data::const_iterator e) {
  data_block result;
  data d;
  for (int i = 1; b != e; ++i, ++b) {
    d.push_back(*b);
    if (i % BLOCK_SIDE == 0) {
      result.push_back(d);
      d.clear();
    }
  }
  return result;
}

data Toy::deconstruct_block(const data_block& block) {
  data result;
  result.reserve(BLOCK_SIZE);

  for (const auto& row : block) {
    result.insert(std::end(result), std::begin(row), std::end(row));
  }

  return result;
}

Toy::data_block Toy::xor_blocks(const data_block& first, const data_block& second) {
  data_block result;

  for (size_t i = 0; i < BLOCK_SIDE; i++) {
    data row;
    const auto& first_row = first.at(i);
    const auto& second_row = second.at(i);
    for (size_t j = 0; j < BLOCK_SIDE; j++) {
      row.push_back(first_row.at(j) ^ second_row.at(j));
    }
    result.push_back(row);
  }

  return result;
}

Toy::data_block Toy::encrypt_permutations(const data_block& block) {
  auto result(block);

  permutation_0(result);
  permutation_1(result);
  permutation_2(result);
  permutation_3(result);
  permutation_4(result);
  permutation_5(result);
  permutation_6(result);
  permutation_7(result);

  return result;
}

Toy::data_block Toy::decrypt_permutations(const data_block& block) {
  auto result(block);

  permutation_7(result);
  permutation_6(result);
  permutation_5(result);
  permutation_4(result);
  permutation_3(result);
  permutation_2(result);
  permutation_1(result);
  permutation_0(result);

  return result;
}

/// @brief if set swaps 0<->3, 1<->2 columns, 0<->2, 1<->3 otherwise
void Toy::permutation_0(data_block& block) {
  auto is_set = key_[0];
  for (int i = 0; i < BLOCK_SIDE; i++) {
    if (is_set) {
      std::swap(block[i][0], block[i][3]);
      std::swap(block[i][1], block[i][2]);
    } else {
      std::swap(block[i][0], block[i][2]);
      std::swap(block[i][1], block[i][3]);
    }
  }
}

/// @brief if set flips block vertical, otherwise horizontal
void Toy::permutation_1(data_block& block) {
  auto is_set = key_[1];
  if (is_set) {
    std::reverse(std::begin(block), std::end(block));
  } else {
    for (auto& row : block) {
      std::reverse(std::begin(row), std::end(row));
    }
  }
}

/// @brief if set swaps 0<->1, 2<->3 rows, 0<->2, 1<->3 otherwise
void Toy::permutation_2(data_block& block) {
  auto is_set = key_[2];
  if (is_set) {
    std::swap(block[0], block[1]);
    std::swap(block[2], block[3]);
  } else {
    std::swap(block[0], block[2]);
    std::swap(block[1], block[3]);
  }
}

/// @brief if set XOR with column 0, otherwise XOR with column 1
void Toy::permutation_3(data_block& block) {
  auto is_set = key_[3];
  for (int i = 0; i < BLOCK_SIDE; i++) {
    if (is_set) {
      block[i][1] = block[i][0] ^ block[i][1];
      block[i][2] = block[i][0] ^ block[i][2];
      block[i][3] = block[i][0] ^ block[i][3];
    } else {
      block[i][0] = block[i][1] ^ block[i][0];
      block[i][2] = block[i][1] ^ block[i][2];
      block[i][3] = block[i][1] ^ block[i][3];
    }
  }

}

/// @brief if set swaps (0,1)<->(2,3) columns, (0,1)<->(2,3) rows otherwise
void Toy::permutation_4(data_block& block) {
  auto is_set = key_[4];
  if (is_set) {
    for (int i = 0; i < BLOCK_SIDE; i++) {
      std::swap(block[i][0], block[i][2]);
      std::swap(block[i][1], block[i][3]);
    }
  } else {
    std::swap(block[0], block[2]);
    std::swap(block[1], block[3]);
  }
}

/// @brief if set XOR with column 0, otherwise XOR with row 1
void Toy::permutation_5(data_block& block) {

  auto is_set = key_[5];
  if (is_set) {
    for (int i = 0; i < BLOCK_SIDE; i++) {
      block[i][1] = block[i][0] ^ block[i][1];
      block[i][2] = block[i][0] ^ block[i][2];
      block[i][3] = block[i][0] ^ block[i][3];
    }
  } else {
    const auto &first_row = block[0];
    for (int i = 1; i < BLOCK_SIDE; i++) {
      auto &row = block[i];
      for (int j = 0; j < BLOCK_SIDE; j++) {
        row[j] = row[j] ^ first_row[j];
      }
    }
  }
}

/// @brief if set XOR each element with key, otherwise XOR element with reversed key
void Toy::permutation_6(data_block& block) {
  auto is_set = key_[6];
  auto key = key_.to_ulong();
  if (!is_set) {
    key = ((key * 0x0802LU & 0x22110LU) | (key * 0x8020LU & 0x88440LU)) * 0x10101LU >> 16;
  }

  for (auto& row : block) {
    for (auto& el : row) {
      el ^= key;
    }
  }
}

/// @brief
void Toy::permutation_7(data_block& block) {
  //TODO implement me
}

} // namespace toy_cipher
