# What is it

It's symmetric block cipher implementation. This cipher is for educational purposes. Don't use it to encrypt sensitive data. The major goal is creating a sandbox for cryptoanalysis.